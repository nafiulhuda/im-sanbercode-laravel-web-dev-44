<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {   //validasi
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|max:255|min:5',
        ]);

        //insert data -> db
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        //return redirect
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast'=>$cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast'=>$cast]);
    }

    public function update($id, Request $request)
    {
        //validasi
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|max:1000|min:5'
        ]);

        //update berdasarkan id data di database
        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request ->input('nama'),
                    'umur' => $request ->input('umur'),
                    'bio' => $request ->input('bio')
                ]
            );

        //return redirect
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
