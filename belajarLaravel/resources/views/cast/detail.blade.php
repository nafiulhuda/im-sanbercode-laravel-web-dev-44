@extends('layouts.master')
@section('title')
    <h1>Halaman Detail Cast</h1>
@endsection

@section('sub-title')
    <h4>Detail Cast</h4>
@endsection

@section('content')
<h1>{{$cast->nama}}</h1>
<h3>{{$cast->umur}}</h3>
<p>{{$cast->bio}}</p>
<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection