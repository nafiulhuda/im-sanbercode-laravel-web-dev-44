@extends('layouts.master')
@section('title')
    <h1>Halaman Tambah Cast</h1>
@endsection

@section('sub-title')
    <h4>Cast</h4>
@endsection

@section('content')
<a href="/category/create" class="btn btn-primary my-3">Tambah</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$value->nama}}</td>
                <td>
                    <form action="/cast/{{$value->id}}" method="POST">
                    <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
           </tr>
        @empty
            <tr>
                <td>
                    Data cast kosong, silahkan tambah data!
                </td>
            </tr>
        @endforelse
      
    </tbody>
  </table>
@endsection