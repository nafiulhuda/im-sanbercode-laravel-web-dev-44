@extends('layouts.master')
@section('title')
    <h1>Halaman Tambah Cast</h1>
@endsection

@section('sub-title')
    <h4>Cast</h4>
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama">
    </div>
    @error('nama')
    <div class="alert alert-danger" role="alert">
        {{$message}}
    </div>
    @enderror
    
    <div class="form-group">
        <label>Umur</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger" role="alert">
        {{$message}}
    </div>
    @enderror

    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control @error('nama') is-invalid @enderror" id="" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger" role="alert">
        {{$message}}
    </div>
    @enderror
    


    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection