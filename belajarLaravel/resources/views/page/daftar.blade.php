@extends('layouts.master')
@section('title')
<h1>Buat Account Baru!</h1>
@endsection

@section('sub-title')
<h4>Sign Up Form</h4>
@endsection

@section('content')
    <form action="/home" method="POST">
        @csrf
        <label> Nama Depan</label><br>
        <input type="text" name="fname"><br><br>
        <label> Nama Belakang</label><br>
        <input type="text" name="lname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male<br><br>
        <input type="radio" name="gender">Female<br><br>
        <input type="radio" name="gender">Other<br><br>
        <label>Nationality:</label><br><br>
        <select>
            <option value="">Indonesia</option>
            <option value="">Malaysia</option>
            <option value="">Singapore</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia<br><br>
        <input type="checkbox" name="bahasa">English<br><br>
        <input type="checkbox" name="bahasa">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">
    </form>
@endsection